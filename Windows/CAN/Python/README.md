Be sure that the following files is in the folder:
PCANBasic.py,
PCANBasic.dll,
ftsensorcan.py

If the PCAN files are missing then download PCANBasic and copy the needed files into the folder

Open er terminal in the this folder and use the following command to run the example.
```sh
$ python ftsensorcan.py
```