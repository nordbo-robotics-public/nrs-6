/***********************************************************************
*
* NRS-6-X CAN Bus example using PCAN.
*
* ----------------------------------------------------------------------
*
* Author: Lars Kristian Feddersen <lars@nordbo-robotics.com>
* Date: 09-10-2018
*
* ----------------------------------------------------------------------
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
************************************************************************/

//Std
#include <Windows.h>
#include <iostream>
#include <string>
#include <chrono>
#include <thread>

//PCANBasic
#define PCAN_DEVICE PCAN_USBBUS1
#include <PCANBasic.h>

//Nordbo Force/Torque CAN constants
//Message classes
#define CAN_MSG_CLASS_POLLING		0x02
#define CAN_MSG_CLASS_PERIODIC		0x03
#define CAN_MSG_CLASS_SYSTEM 		0x07

//Polling messages
#define CAN_CMD_GET_CALIBRATION_RANGE   0x51
#define CAN_CMD_SET_TX			0x07
#define CAN_CMD_SET_TX_START		0x00
#define CAN_CMD_SET_TX_STOP		0x01
#define CAN_CMD_SET_TARE	        0x15
#define CAN_CMD_SET_TARE_NEGATIVE    	0x01

//Periodic message
#define CAN_CMD_FORCE_VECTOR            0x0A
#define CAN_CMD_TORQUE_VECTOR           0x0B

//System messages
#define CAN_CMD_BROADCAST		0xFF

//Common addresses
#define CAN_ADDR_BROADCAST      	0x0F
#define CAN_ADDR_PC             	0x00

//Helper functions
#define GET_MSG_CLASS(ID)              		 	((ID & 0x700) >> 8)
#define GET_SRC_ADDR(ID)                		((ID & 0x0F0) >> 4)
#define GET_DST_ADDR(ID)                		(ID & 0x00F)
#define GET_CAN_ID(MSG_CLASS, SRC_ADDR, DST_ADDR)	((MSG_CLASS << 8) | (SRC_ADDR << 4) | DST_ADDR)

unsigned char CANAddr = 0;
unsigned int CRFxy = 0, CRFz  = 0, CRTxy = 0, CRTz  = 0;

void getFTSensorCANAddr();
void getFTSensorCalibrationRange();
void startTransmission();
void tare();
void stopTransmission();
TPCANMsg readCANMsg();
void writeCANMsg(TPCANMsg &canMsg);
double convertFTData(unsigned char LSB, unsigned char MSB, unsigned int cr);
void printCANStatus(std::string msg, TPCANStatus canStatus);
void printCANMsg(const TPCANMsg &canMsg);

int main(int argc, char **argv)
{
	TPCANMsg canMsg;
	TPCANStatus canStatus;

	canStatus = CAN_Initialize(PCAN_DEVICE, PCAN_BAUD_1M, PCAN_PEAKCAN, 0, 0);
	printCANStatus("CAN_Initialize", canStatus);

	if(canStatus != PCAN_ERROR_OK) {
		return 1;
	}

	//Send a broadcast message to get the CANID of the NRS-6-X.
	getFTSensorCANAddr(); 

	//Before using the Force/Torque values transmitted by NRS-6-X we need the
	//calibration range in order to convert the values into Newton and Torque. 
	getFTSensorCalibrationRange();

	//This will use the negative value of the currently readings and zero the sensor.
	tare();

	startTransmission();

	double Fx, Fy, Fz, Tx, Ty, Tz;
    for(size_t i = 0; i < 10000; ++i) {
		canMsg = readCANMsg();

		if(GET_DST_ADDR(canMsg.ID) == CAN_CMD_FORCE_VECTOR) {
			Fx = convertFTData(canMsg.DATA[0], canMsg.DATA[1], CRFxy);
			Fy = convertFTData(canMsg.DATA[2], canMsg.DATA[3], CRFxy);
			Fz = convertFTData(canMsg.DATA[4], canMsg.DATA[5], CRFz);
		} else if(GET_DST_ADDR(canMsg.ID) == CAN_CMD_TORQUE_VECTOR) {
			Tx = convertFTData(canMsg.DATA[0], canMsg.DATA[1], CRTxy);
			Ty = convertFTData(canMsg.DATA[2], canMsg.DATA[3], CRTxy);
			Tz = convertFTData(canMsg.DATA[4], canMsg.DATA[5], CRTz);

			std::cout << Fx << " " << Fy << " " << Fz  << " " << Tx << " " << Ty << " " << Tz << std::endl;
		}
	}

	stopTransmission();

	canStatus = CAN_Uninitialize(PCAN_DEVICE);
	printCANStatus("CAN_Uninitialize", canStatus);
	
	return 0;
}

double convertFTData(unsigned char LSB, unsigned char MSB, unsigned int cr)
{
	return (32767.-static_cast<double>(LSB + MSB*256)) * static_cast<double>(cr)/32768.;
}

void startTransmission()
{
	TPCANMsg canMsg;

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr);
	canMsg.LEN = 2;
	canMsg.MSGTYPE  = PCAN_MESSAGE_STANDARD;
	canMsg.DATA[0] = CAN_CMD_SET_TX;
	canMsg.DATA[1] = CAN_CMD_SET_TX_START;
	
	writeCANMsg(canMsg);
	canMsg = readCANMsg();
	
	std::cout << "Force/Torque sensor transmission started" << std::endl << std::endl;
}

void stopTransmission()
{
	TPCANMsg canMsg;

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr);
	canMsg.LEN = 2;
	canMsg.MSGTYPE  = PCAN_MESSAGE_STANDARD;
	canMsg.DATA[0] = CAN_CMD_SET_TX;
	canMsg.DATA[1] = CAN_CMD_SET_TX_STOP;
	
	writeCANMsg(canMsg);

	//Wait until and ACK msg is send back for the stop command. 
	do {
		canMsg = readCANMsg();
	} while(GET_MSG_CLASS(canMsg.ID) != CAN_MSG_CLASS_POLLING);

	std::cout << "Force/Torque sensor transmission stopped" << std::endl << std::endl;
}

void tare()
{
	TPCANMsg canMsg;

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr);
	canMsg.LEN = 2;
	canMsg.MSGTYPE  = PCAN_MESSAGE_STANDARD;
	canMsg.DATA[0] = CAN_CMD_SET_TARE;
	canMsg.DATA[1] = CAN_CMD_SET_TARE_NEGATIVE;
	
	writeCANMsg(canMsg);
	canMsg = readCANMsg();
	
	std::cout << "Force/Torque sensor tare" << std::endl << std::endl;	
}

void getFTSensorCalibrationRange()
{
	TPCANMsg canMsg;

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr);
	canMsg.LEN = 1;
	canMsg.MSGTYPE  = PCAN_MESSAGE_STANDARD;
	canMsg.DATA[0] = CAN_CMD_GET_CALIBRATION_RANGE;
	
	writeCANMsg(canMsg);
	canMsg = readCANMsg();

        CRFxy = (canMsg.DATA[1] << 8) | canMsg.DATA[2];
        CRFz  = (canMsg.DATA[3] << 8) | canMsg.DATA[4];
        CRTxy = canMsg.DATA[5];
        CRTz  = canMsg.DATA[6];

	std::cout << "Force/Torque sensor calibration range: Fxy: " << CRFxy << " Fz: " << CRFz << " Txy: " << CRTxy << " Tz: " << CRTz << std::endl << std::endl;
}

void getFTSensorCANAddr()
{
	TPCANMsg canMsg;

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_SYSTEM, CAN_ADDR_PC, CAN_ADDR_BROADCAST);
	canMsg.LEN = 1;
	canMsg.MSGTYPE  = PCAN_MESSAGE_STANDARD;
	canMsg.DATA[0] = CAN_CMD_BROADCAST;
	
	writeCANMsg(canMsg);
	canMsg = readCANMsg();
	
	CANAddr = GET_SRC_ADDR(canMsg.ID);
	std::cout << "Force/Torque sensor found. CAN Address: " << (int)CANAddr << std::endl << std::endl;
}

void writeCANMsg(TPCANMsg &canMsg)
{	
	TPCANStatus canStatus = CAN_Write(PCAN_DEVICE, &canMsg);
	printCANStatus("CAN_Write", canStatus);
	printCANMsg(canMsg);	
}

TPCANMsg readCANMsg()
{
	TPCANStatus canStatus;
	TPCANMsg canMsg;
	
	//Polling is not recommend as PCAN is providing an event base impletation. 
	//However, polling is used for simplicity. 
	while ((canStatus = CAN_Read(PCAN_DEVICE, &canMsg, NULL)) == PCAN_ERROR_QRCVEMPTY)
        std::this_thread::sleep_for(std::chrono::microseconds(10));
	
	printCANStatus("CAN_Read", canStatus);
	printCANMsg(canMsg);

	return canMsg;
}

void printCANMsg(const TPCANMsg &canMsg)
{
	std::cout << "MSG_CLASS: " << GET_MSG_CLASS(canMsg.ID) 
		  << " SRC_ADDR: " << GET_SRC_ADDR(canMsg.ID)
		  << " DST_ADDR: " << GET_DST_ADDR(canMsg.ID)
		  << std::endl 
                  << "DATA: ";
	for(size_t i = 0; i < canMsg.LEN; ++i) {
		std::cout << (int)canMsg.DATA[i] << " ";
	}
	std::cout << std::endl << std::endl;
}

void printCANStatus(std::string msg, TPCANStatus canStatus)
{
	char canErrorMsg[256]; // 256 is PCAN error max size	
	CAN_GetErrorText(canStatus, 0, canErrorMsg);
	std::cout << msg << ": " << canErrorMsg << std::endl;	
}
