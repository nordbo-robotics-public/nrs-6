Be sure that the following files is in the folder:
peak : contains PCAN files
PCANBasic.dll
PCANBasic_JNI.dll
FTSensorCan.java

If the PCAN files are missing then download PCANBasic and copy the needed files into the folder

Open er terminal in this folder and use the following commands to compile and run the example.
```sh
$ javac -d . ./basic/*.java
$ javac FTSensorCan.java 
$ java FTSensorCan
```