/***********************************************************************
*
* NRS-6-X CAN Bus examples using PCAN.
*
* ----------------------------------------------------------------------
*
* Author: Lars Kristian Feddersen <lars@nordbo-robotics.com>
* Date: 09-10-2018
*
* ----------------------------------------------------------------------
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
************************************************************************/

import peak.can.basic.*;

public class FTSensorCan {

	//Nordbo Force/Torque CAN constants
	//Message classes
	private static final byte CAN_MSG_CLASS_POLLING		= (byte)0x02;
	private static final byte CAN_MSG_CLASS_PERIODIC	= (byte)0x03;
	private static final byte CAN_MSG_CLASS_SYSTEM 		= (byte)0x07;

	//Polling messages
	private static final byte CAN_CMD_GET_CALIBRATION_RANGE = (byte)0x51;
	private static final byte CAN_CMD_SET_TX		= (byte)0x07;
	private static final byte CAN_CMD_SET_TX_START		= (byte)0x00;
	private static final byte CAN_CMD_SET_TX_STOP		= (byte)0x01;
	private static final byte CAN_CMD_SET_TARE	        = (byte)0x15;
	private static final byte CAN_CMD_SET_TARE_NEGATIVE    	= (byte)0x01;

	//Periodic message
	private static final byte CAN_CMD_FORCE_VECTOR           = (byte)0x0A;
	private static final byte CAN_CMD_TORQUE_VECTOR          = (byte)0x0B;

	//System messages
	private static final byte CAN_CMD_BROADCAST		= (byte)0xFF;

	//Common addresses
	private static final byte CAN_ADDR_BROADCAST      	= (byte)0x0F;
	private static final byte CAN_ADDR_PC             	= (byte)0x00;

	private static PCANBasic CAN = null;
	private static TPCANHandle PCAN_DEVICE = TPCANHandle.PCAN_USBBUS1;
	private static int CANAddr = 0;
	private static int CRFxy = 0, CRFz  = 0, CRTxy = 0, CRTz  = 0;

	public static void main(String[] args) {
		TPCANStatus canStatus = null;
		TPCANMsg canMsg = null;

		CAN = new PCANBasic();
		CAN.initializeAPI();
		canStatus = CAN.Initialize(PCAN_DEVICE, TPCANBaudrate.PCAN_BAUD_1M, TPCANType.PCAN_TYPE_NONE, 0, (short)0);
		printCANStatus("CAN.Initialize", canStatus);
		
		if(canStatus != TPCANStatus.PCAN_ERROR_OK) {
			System.exit(1);
		}

		//Read the status message send by the PCAN.
		//This is not needed if it is running on windows
		if (!System.getProperty("os.name").startsWith("Windows"))
			canMsg = readCANMsg();

		//Send a broadcast message to get the CANID of the NRS-6-X.
		getFTSensorCANAddr();

		//Before using the Force/Torque values transmitted by NRS-6-X we need the
		//calibration range in order to convert the values into Newton and Torque. 
		getFTSensorCalibrationRange();

		//This will use the negative value of the currently readings and zero the sensor.
		tare();

		startTransmission();

		double Fx = 0., Fy = 0., Fz = 0., Tx = 0., Ty = 0., Tz = 0.;

		for(int i = 0; i < 1000; i++) {
			canMsg = readCANMsg();

			if(GET_DST_ADDR(canMsg.getID()) == CAN_CMD_FORCE_VECTOR) {
				Fx = convertFTData(canMsg.getData()[0], canMsg.getData()[1], CRFxy);
				Fy = convertFTData(canMsg.getData()[2], canMsg.getData()[3], CRFxy);
				Fz = convertFTData(canMsg.getData()[4], canMsg.getData()[5], CRFz);
			} else if(GET_DST_ADDR(canMsg.getID()) == CAN_CMD_TORQUE_VECTOR) {
				Tx = convertFTData(canMsg.getData()[0], canMsg.getData()[1], CRTxy);
				Ty = convertFTData(canMsg.getData()[2], canMsg.getData()[3], CRTxy);
				Tz = convertFTData(canMsg.getData()[4], canMsg.getData()[5], CRTz);

				System.out.println(String.valueOf(Fx) + " " + String.valueOf(Fy) + " " + String.valueOf(Fz)  + " " + String.valueOf(Tx) + " " + String.valueOf(Ty) + " " + String.valueOf(Tz) + "\n");
			}
		}

		stopTransmission();
		canStatus = CAN.Uninitialize(PCAN_DEVICE);
		printCANStatus("CAN.Uninitialize", canStatus);
		System.exit(0);
	}

	private static double convertFTData(byte LSB, byte MSB, int cr) {
		int data = ((MSB << 8) & 0x0000ff00) | (LSB & 0x000000ff);
		return (32767.-(double)data) * (double)cr/32768.;
	}

	private static void startTransmission() {
		TPCANMsg canMsg = new TPCANMsg();
	
		canMsg.setID(GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr));
		canMsg.setType(TPCANMessageType.PCAN_MESSAGE_STANDARD);
		byte data[] = new byte[2];
		data[0] = CAN_CMD_SET_TX;
		data[1] = CAN_CMD_SET_TX_START;
		canMsg.setData(data, (byte)2);
	
		writeCANMsg(canMsg);
		canMsg = readCANMsg();		
	
		System.out.println("Force/Torque sensor transmission started\n");		
	}
	
	private static void stopTransmission() {
		TPCANMsg canMsg = new TPCANMsg();
	
		canMsg.setID(GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr));
		canMsg.setType(TPCANMessageType.PCAN_MESSAGE_STANDARD);
		byte data[] = new byte[2];
		data[0] = CAN_CMD_SET_TX;
		data[1] = CAN_CMD_SET_TX_STOP;
		canMsg.setData(data, (byte)2);
	
		writeCANMsg(canMsg);

		//Wait until and ACK msg is send back for the stop command. 
		do {
			canMsg = readCANMsg();
		} while(GET_MSG_CLASS(canMsg.getID()) != CAN_MSG_CLASS_POLLING && canMsg.getType() != 0);	
	
		System.out.println("Force/Torque sensor transmission stopped\n");		
	}

	private static void tare() {
		TPCANMsg canMsg = new TPCANMsg();
	
		canMsg.setID(GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr));
		canMsg.setType(TPCANMessageType.PCAN_MESSAGE_STANDARD);
		byte data[] = new byte[2];
		data[0] = CAN_CMD_SET_TARE;
		data[1] = CAN_CMD_SET_TARE_NEGATIVE;
		canMsg.setData(data, (byte)2);
	
		writeCANMsg(canMsg);
		canMsg = readCANMsg();
	
		System.out.println("Force/Torque sensor tare\n");
	}

	private static void getFTSensorCalibrationRange() {
		TPCANMsg canMsg = new TPCANMsg();
	
		canMsg.setID(GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr));
		canMsg.setType(TPCANMessageType.PCAN_MESSAGE_STANDARD);
		byte data[] = new byte[1];
		data[0] = CAN_CMD_GET_CALIBRATION_RANGE;
		canMsg.setData(data, (byte)1);
	
		writeCANMsg(canMsg);
		canMsg = readCANMsg();
	
		CRFxy = ((canMsg.getData()[1] << 8) & 0x0000ff00) | (canMsg.getData()[2] & 0x000000ff);
		CRFz  = ((canMsg.getData()[3] << 8) & 0x0000ff00) | (canMsg.getData()[4] & 0x000000ff);
		CRTxy = (canMsg.getData()[5] & 0x000000ff);
		CRTz  = (canMsg.getData()[6] & 0x000000ff);

		System.out.println("Force/Torque sensor calibration range: Fxy: " + String.valueOf(CRFxy) + " Fz: " + String.valueOf(CRFz) + " Txy: " + String.valueOf(CRTxy) + " Tz: " + String.valueOf(CRTz) + "\n");		
	}

	private static void getFTSensorCANAddr() {
		TPCANMsg canMsg = new TPCANMsg();
	
		canMsg.setID(GET_CAN_ID(CAN_MSG_CLASS_SYSTEM, CAN_ADDR_PC, CAN_ADDR_BROADCAST));
		canMsg.setType(TPCANMessageType.PCAN_MESSAGE_STANDARD);
		byte data[] = new byte[1];
		data[0] = CAN_CMD_BROADCAST;
		canMsg.setData(data, (byte)1);
	
		writeCANMsg(canMsg);
		canMsg = readCANMsg();
	
		CANAddr = GET_SRC_ADDR(canMsg.getID());
		System.out.println("Force/Torque sensor found. CAN Address: " + String.valueOf(CANAddr) + "\n");
	}

	private static void writeCANMsg(TPCANMsg canMsg) {
		TPCANStatus canStatus = CAN.Write(PCAN_DEVICE, canMsg);
		printCANStatus("CAN.Write", canStatus);
		printCANMsg(canMsg);
	}

	private static TPCANMsg readCANMsg() {
		TPCANStatus canStatus = null;
		TPCANMsg canMsg = new TPCANMsg();
		TPCANTimestamp canTime = new TPCANTimestamp();
		
		//Polling is not recommend as PCAN is providing an event base impletation. 
		//However, polling is used for simplicity. 
		do {
			canStatus = CAN.Read(PCAN_DEVICE, canMsg, canTime);
		} while(canStatus == TPCANStatus.PCAN_ERROR_QRCVEMPTY);

		printCANStatus("CAN.Read", canStatus);
		printCANMsg(canMsg);

		return canMsg;
	}

	private static int GET_MSG_CLASS(int ID) {
		return ((ID & 0x700) >> 8);
	}

	private static int GET_SRC_ADDR(int ID) {
		return ((ID & 0x0F0) >> 4);
	}

	private static int GET_DST_ADDR(int ID) {
		return (ID & 0x00F);
	}

	private static int GET_CAN_ID(int MSG_CLASS, int SRC_ADDR, int DST_ADDR) {
		return ((MSG_CLASS << 8) | (SRC_ADDR << 4) | DST_ADDR);
	}

	private static void printCANMsg(TPCANMsg canMsg) {
		System.out.println("MSG_CLASS: " + String.valueOf(GET_MSG_CLASS(canMsg.getID())) + 
				   " SRC_ADDR: " + String.valueOf(GET_SRC_ADDR(canMsg.getID())) + 
				   " DST_ADDR: " + String.valueOf(GET_DST_ADDR(canMsg.getID())));
		String dataStr = "DATA: ";

		for(int i = 0; i < canMsg.getLength(); i++){
			dataStr += String.valueOf((int)canMsg.getData()[i] & 0xFF) + " ";
		}

		dataStr += "\n";
		System.out.println(dataStr);
	}

	private static void printCANStatus(String msg, TPCANStatus canStatus) {
		StringBuffer canErrorMsg = new StringBuffer("");
		CAN.GetErrorText(canStatus, (short)0, canErrorMsg);
		System.out.println(msg + ": " + canErrorMsg.toString());
	}
}
