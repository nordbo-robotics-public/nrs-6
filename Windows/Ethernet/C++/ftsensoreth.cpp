/***********************************************************************
*
* NRS-6-X Ethernet TCP/IP example.
*
* ----------------------------------------------------------------------
*
* Author: Lars Kristian Feddersen <lars@nordbo-robotics.com>
* Date: 09-10-2018
*
* ----------------------------------------------------------------------
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
************************************************************************/

//Std
#pragma comment(lib, "ws2_32.lib")
#include <iostream>

//Windows Socket
#include <winsock2.h>
#include <ws2tcpip.h>


#define IP_ADDR "192.168.0.109"
#define PORT	2001

#define CMD_TYPE_SENSOR_TRANSMIT 	0x07
#define SENSOR_TRANSMIT_TYPE_START	0x01
#define SENSOR_TRANSMIT_TYPE_STOP	0x00

#define CMD_TYPE_SET_CURRENT_TARE 	0x15
#define SET_CURRENT_TARE_TYPE_NEGATIVE	0x01

void printMsg(const char *msg);
void recvMsg(char *msg);
double bytesToDouble(const char *bytes);

SOCKET socket_desc;

int main()
{
    WSADATA wsadata;
    int error = WSAStartup(0x0202, &wsadata);
    if (wsadata.wVersion != 0x0202)
    {
        WSACleanup(); //Clean up Winsock
        return false;
    }

    socket_desc = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socket_desc < 0)
      std::cout << "error opening socket" << std::endl;
	struct sockaddr_in server;

	server.sin_addr.s_addr = inet_addr(IP_ADDR);
    server.sin_family = AF_INET;
	server.sin_port = htons(PORT);

 
    std::cout << "Connecting..." << std::endl;

    if(error = connect(socket_desc , (SOCKADDR*)&server , sizeof(server))) {
        std::cout << "Error code: " << error << std::endl;
		return 1;
	}

	std::cout << "Connected to the CANToEth Module" << std::endl;

	char recvData[50] = {0};
	char sendData[3] = {0};

	sendData[0] = 0x03;
	sendData[1] = CMD_TYPE_SET_CURRENT_TARE;
	sendData[2] = SET_CURRENT_TARE_TYPE_NEGATIVE;
	send(socket_desc, sendData, sendData[0], 0);
	recvMsg(recvData);	

	sendData[0] = 0x03;
	sendData[1] = CMD_TYPE_SENSOR_TRANSMIT;
	sendData[2] = SENSOR_TRANSMIT_TYPE_START;
	send(socket_desc, sendData, sendData[0], 0);
	recvMsg(recvData);
	
	double Fx, Fy, Fz, Tx, Ty, Tz;
    for(size_t i = 0; i < 10000; ++i) {
		recvMsg(recvData);
		Fx = bytesToDouble(&recvData[2]);
		Fy = bytesToDouble(&recvData[10]);
		Fz = bytesToDouble(&recvData[18]);
		Tx = bytesToDouble(&recvData[26]);
		Ty = bytesToDouble(&recvData[34]);
		Tz = bytesToDouble(&recvData[42]);

		std::cout << Fx << " " << Fy << " " << Fz << " " << Tx << " " << Ty << " " << Tz << std::endl;	
	}
	sendData[0] = 0x03;
	sendData[1] = CMD_TYPE_SENSOR_TRANSMIT;
	sendData[2] = SENSOR_TRANSMIT_TYPE_STOP;
	send(socket_desc, sendData, sendData[0], 0);

	//Wait until and ACK msg is send back for the stop command. 
	do {
		recvMsg(recvData);
	} while(recvData[0] != 3 && recvData[1] != CMD_TYPE_SENSOR_TRANSMIT);

	closesocket(socket_desc);
	

	return 0;
}

double bytesToDouble(const char *bytes)
{
	double data;

    unsigned char* ptr = (unsigned char*)(&data);
	for(int i = 7; i >= 0; --i) {
		*ptr = bytes[i] & 0xFF;
		ptr++;
	}

	return data;
}

void recvMsg(char *msg) {
	size_t received = recv(socket_desc, msg, 2, 0);
	
	while(received < msg[0]) {
		received += recv(socket_desc, &msg[received], msg[0] - received, 0);
	}	

	printMsg(msg);	
}

void printMsg(const char *msg)
{
	std::cout << "Msg len: " << (int)msg[0]	<< " Msg type: " << (int)msg[1] << std::endl << "Body: ";
	
	for(size_t i = 0; i < msg[0] - 2; ++i) {
        std::cout << (int)(uint8_t)msg[i + 2] << " ";
	}

	std::cout << std::endl << std::endl;
};
