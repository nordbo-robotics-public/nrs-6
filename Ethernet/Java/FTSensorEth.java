/***********************************************************************
*
* NRS-6-X Ethernet TCP/IP example.
*
* ----------------------------------------------------------------------
*
* Author: Lars Kristian Feddersen <lars@nordbo-robotics.com>
* Date: 09-10-2018
*
* ----------------------------------------------------------------------
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
************************************************************************/

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class FTSensorEth {

	private static final String IP_ADDR 	= "192.168.0.109";
	private static final int PORT		= 2001;

	private static final byte CMD_TYPE_SENSOR_TRANSMIT 	= 0x07;
	private static final byte SENSOR_TRANSMIT_TYPE_START 	= 0x01;
	private static final byte SENSOR_TRANSMIT_TYPE_STOP 	= 0x00;

	private static final byte CMD_TYPE_SET_CURRENT_TARE 	= 0x15;
	private static final byte SET_CURRENT_TARE_TYPE_NEGATIVE= 0x01;

	private static Socket socket = null;
	private static OutputStream output = null;	
	private static InputStream input = null;

	public static void main(String[] args) throws IOException {
		socket = new Socket(IP_ADDR, PORT);
		output = socket.getOutputStream();
		input = socket.getInputStream();
		
		byte[] recvData = new byte[50];
		byte[] sendData = new byte[3];

		sendData[0] = 0x03;
		sendData[1] = CMD_TYPE_SET_CURRENT_TARE;
		sendData[2] = SET_CURRENT_TARE_TYPE_NEGATIVE;
		output.write(sendData);
		recvData = recvMsg();

		sendData[0] = 0x03;
		sendData[1] = CMD_TYPE_SENSOR_TRANSMIT;
		sendData[2] = SENSOR_TRANSMIT_TYPE_START;
		output.write(sendData);
		recvData = recvMsg();

		double Fx, Fy, Fz, Tx, Ty, Tz;
		for(int i = 0; i < 10000; ++i) {
			recvData = recvMsg();
			Fx = ByteBuffer.wrap(Arrays.copyOfRange(recvData, 2, 10)).getDouble();
			Fy = ByteBuffer.wrap(Arrays.copyOfRange(recvData, 10, 18)).getDouble();
			Fz = ByteBuffer.wrap(Arrays.copyOfRange(recvData, 18, 26)).getDouble();
			Tx = ByteBuffer.wrap(Arrays.copyOfRange(recvData, 26, 34)).getDouble();
			Ty = ByteBuffer.wrap(Arrays.copyOfRange(recvData, 34, 42)).getDouble();
			Tz = ByteBuffer.wrap(Arrays.copyOfRange(recvData, 42, 50)).getDouble();
			System.out.println(" " + String.valueOf(Fx) + " " + String.valueOf(Fy) + " " + String.valueOf(Fz) +
					   " " + String.valueOf(Tx) + " " + String.valueOf(Ty) + " " + String.valueOf(Tz));
		}

		sendData[0] = 0x03;
		sendData[1] = CMD_TYPE_SENSOR_TRANSMIT;
		sendData[2] = SENSOR_TRANSMIT_TYPE_STOP;
		output.write(sendData);

		//Wait until and ACK msg is send back for the stop command. 
		do {
			recvData = recvMsg();
		} while(recvData[0] != 3 && recvData[1] != CMD_TYPE_SENSOR_TRANSMIT);
		
		output.close();
		input.close();
		socket.close();
		System.exit(0);
	}

	private static byte[] recvMsg() throws IOException {
		byte[] recvData = new byte[50];
		int received = input.read(recvData, 0, 2);
		
		while(received < recvData[0]) {
			received += input.read(recvData, received, recvData[0] - received);
		}

		printMsg(recvData);

		return recvData;
	}

	private static void printMsg(byte[] msg) {
		System.out.println("Msg len: " + String.valueOf(msg[0]) + " Msg type: " + String.valueOf(msg[1]));

		String dataStr = "DATA: ";
		for(int i = 0; i < msg[0] - 2; i++){
			dataStr += String.valueOf((int)msg[i + 2] & 0xFF) + " ";
		}
		dataStr += "\n";
		System.out.println(dataStr);
	}

};