Find the correct IP-address of the CANToEth module and set it in the example before compiling.
Open a terminal in the current directory and use the following commands to compile and run the example. 
```sh
$ g++ ftsensoreth.cpp -o ftsensoreth
$ ./ftsensoreth
```