# Simple Code Examples for NRS-6050 and NRS-6200 Force/Torque Sensor from Nordbo Robotics.
This repository contains a few simple code examples, in different programming languages, that show how to interface to the NRS-6-X using CAN bus or Ethernet TCP/IP. It should be noted that the code examples are only for convenience and are not optimized. A README is located in each example that descripe how to execute the specific example. 

## 1 Test Platform and Dependencies

All examples are tested on Xubuntu 16.04 using:
- GCC 5.4.0 for the C++ examples.
- OpenJDK 8 for the Java examples.
- Python 2.7.12 for the Python examples. 

However, with none or minor modifications all examples should be able to be executed on other Linux distributions and even Windows. Furthermore, other versions of the above should work as well. 

#### 1.1 CAN Bus Examples
The CAN bus examples are utilizing the PCAN-USB adapter from [PEAK System](https://www.peak-system.com). However, it should be noted that any CAN enabled device able to communicate at 1 Mbit/s can interface to the NRS-6-X. 

A PCAN-USB adapter and the most recently PCAN Linux driver is required in order to execute the examples. Furthermore, is the most recently PCAN-Basic library for Linux required as the examples are using the API for interfacing to the PCAN-USB adapter. 

The CAN bus examples are tested with the following versions:
- PCAN Driver for Linux version 8.6.x
- PCAN-Basic library for Linux version 4.2.x

A PCAN Driver and PCAN-Basic library for Windows is available as well and other version should work as well.

## 2 Contents of this Repository
```
+-- README.md
    +-- CAN
        +-- C++
            +-- ftsensorcan.cpp
            +-- README.md
        +-- Java
            +-- FTSensorCan.java
            +-- README.md
        +-- Python
            +-- ftsensorcan.py
            +-- README.md
    +-- Ethernet
        +-- C++
            +-- ftsensoreth.cpp
            +-- README.md
        +-- Java
            +-- FTSensorEth.java
            +-- README.md
        +-- Python
            +-- ftsensoreth.py
            +-- README.md
```
