# NRS-6 CODESYS Library

## 1 Test platform
NBRFT.lib is created and tested with Schneider Machine Expert v1.2

## 2 Content
The content of the NBRFT 1.1.0.1 is as follows:

```
+-- FTSensor
    +--DAC
        +--FB_NordboFTDAC
    +--Ethernet version
        +--FB_NordboFTEth
```

## 3 Function block descriptions
FB_NordboFTDAC : Digital to analog conversion function block

FB_NordboFTEth : Ethernet protocol function block
