Copy the `basic` folder from the PCAN Basic Java example folder to this folder.
Open er terminal in the this folder and use the following commands to compile and run the example.
```sh
$ javac -d . ./basic/*.java
$ javac FTSensorCan.java 
$ java FTSensorCan
```