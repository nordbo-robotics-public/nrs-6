#***********************************************************************
#
# NRS-6-X CAN Bus example using PCAN.
#
# ----------------------------------------------------------------------
#
# Author: Lars Kristian Feddersen <lars@nordbo-robotics.com>
# Date: 09-10-2018
#
# ----------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#***********************************************************************/

from PCANBasic import *
import time

#Nordbo Force/Torque CAN constants
#Message classes
CAN_MSG_CLASS_POLLING		= int(0x02)
CAN_MSG_CLASS_PERIODIC		= int(0x03)
CAN_MSG_CLASS_SYSTEM 		= int(0x07)

#Polling messages
CAN_CMD_GET_CALIBRATION_RANGE   = int(0x51)
CAN_CMD_SET_TX			= int(0x07)
CAN_CMD_SET_TX_START		= int(0x00)
CAN_CMD_SET_TX_STOP		= int(0x01)
CAN_CMD_SET_TARE	        = int(0x15)
CAN_CMD_SET_TARE_NEGATIVE    	= int(0x01)

#Periodic message
CAN_CMD_FORCE_VECTOR            = int(0x0A)
CAN_CMD_TORQUE_VECTOR           = int(0x0B)

#System messages
CAN_CMD_BROADCAST		= int(0xFF)

#Common addresses
CAN_ADDR_BROADCAST      	= int(0x0F)
CAN_ADDR_PC             	= int(0x00)

PCAN_DEVICE = PCAN_USBBUS1
CAN = PCANBasic()

CANAddr = int(0)
CRFxy = int(0)
CRFz  = int(0)
CRTxy = int(0)
CRTz  = int(0)

def main():
	canMsg = TPCANMsg()
	canStatus = CAN.Initialize(PCAN_DEVICE, PCAN_BAUD_1M, 0, 0, 0)
	printCANStatus("CAN.Initialize", canStatus)

	if canStatus != PCAN_ERROR_OK:
		return 1

	#Read the status message send by the PCAN.
	canMsg = readCANMsg()

	#Send a broadcast message to get the CANID of the NRS-6-X.
	getFTSensorCANAddr()

	#Before using the Force/Torque values transmitted by NRS-6-X we need the
	#calibration range in order to convert the values into Newton and Torque.
	getFTSensorCalibrationRange()

	#This will use the negative value of the currently readings and zero the sensor.
	tare()
	startTransmission()

	for i in range(10000):
		canMsg = readCANMsg()
		if GET_DST_ADDR(canMsg.ID) == CAN_CMD_FORCE_VECTOR:
			Fx = convertFTData(canMsg.DATA[0], canMsg.DATA[1], CRFxy)
			Fy = convertFTData(canMsg.DATA[2], canMsg.DATA[3], CRFxy)
			Fz = convertFTData(canMsg.DATA[4], canMsg.DATA[5], CRFz)
		elif GET_DST_ADDR(canMsg.ID) == CAN_CMD_TORQUE_VECTOR :
			Tx = convertFTData(canMsg.DATA[0], canMsg.DATA[1], CRTxy)
			Ty = convertFTData(canMsg.DATA[2], canMsg.DATA[3], CRTxy)
			Tz = convertFTData(canMsg.DATA[4], canMsg.DATA[5], CRTz)

			print(str(Fx) + " " + str(Fy) + " " + str(Fz) + " " + str(Tx) + " " + str(Ty) + " " + str(Tz));

	stopTransmission()

	canStatus = CAN.Uninitialize(PCAN_DEVICE)
	printCANStatus("CAN.Uninitialize", canStatus)

	return 0

def convertFTData(LSB, MSB, cr):
	return (32767.-(LSB + MSB*256)) * cr/32768.

def startTransmission():
	canMsg = TPCANMsg()

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr)
	canMsg.LEN = 2
	canMsg.MSGTYPE  = PCAN_MESSAGE_STANDARD
	canMsg.DATA[0] = CAN_CMD_SET_TX
	canMsg.DATA[1] = CAN_CMD_SET_TX_START
	
	writeCANMsg(canMsg)
	canMsg = readCANMsg()
	
	print("Force/Torque sensor transmission started\n")

def stopTransmission():
	canMsg = TPCANMsg()

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr)
	canMsg.LEN = 2
	canMsg.MSGTYPE  = PCAN_MESSAGE_STANDARD
	canMsg.DATA[0] = CAN_CMD_SET_TX
	canMsg.DATA[1] = CAN_CMD_SET_TX_STOP
	
	writeCANMsg(canMsg)

	#Wait until and ACK msg is send back for the stop command. 
	canMsg = readCANMsg()
	while(GET_MSG_CLASS(canMsg.ID) != CAN_MSG_CLASS_POLLING):
		canMsg = readCANMsg()

	print("Force/Torque sensor transmission stopped\n")

def tare():
	canMsg = TPCANMsg()

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr)
	canMsg.LEN = 2
	canMsg.MSGTYPE  = PCAN_MESSAGE_STANDARD
	canMsg.DATA[0] = CAN_CMD_SET_TARE
	canMsg.DATA[1] = CAN_CMD_SET_TARE_NEGATIVE
	
	writeCANMsg(canMsg)
	canMsg = readCANMsg()
	
	print("Force/Torque sensor tare\n")

def getFTSensorCalibrationRange():
	global CRFxy, CRFz, CRTxy, CRTz
	canMsg = TPCANMsg()

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_POLLING, CAN_ADDR_PC, CANAddr)
	canMsg.LEN = 1
	canMsg.MSGTYPE = PCAN_MESSAGE_STANDARD
	canMsg.DATA[0] = CAN_CMD_GET_CALIBRATION_RANGE
	
	writeCANMsg(canMsg)
	canMsg = readCANMsg()

        CRFxy = (canMsg.DATA[1] << 8) | canMsg.DATA[2];
        CRFz  = (canMsg.DATA[3] << 8) | canMsg.DATA[4];
        CRTxy = canMsg.DATA[5];
        CRTz  = canMsg.DATA[6];

	print("Force/Torque sensor calibration range: Fxy: " + str(CRFxy) + " Fz: " + str(CRFz) + " Txy: " + str(CRTxy) + " Tz: " + str(CRTz) + "\n")

def getFTSensorCANAddr():
	global CANAddr
	canMsg = TPCANMsg()

	canMsg.ID = GET_CAN_ID(CAN_MSG_CLASS_SYSTEM, CAN_ADDR_PC, CAN_ADDR_BROADCAST)
	canMsg.LEN = 1
	canMsg.MSGTYPE = PCAN_MESSAGE_STANDARD
	canMsg.DATA[0] = CAN_CMD_BROADCAST
	
	writeCANMsg(canMsg);
	canMsg = readCANMsg();
	
	CANAddr = GET_SRC_ADDR(canMsg.ID);

	print("Force/Torque sensor found. CAN Address: " + str(CANAddr) + "\n");

def writeCANMsg(canMsg):
	canStatus = CAN.Write(PCAN_DEVICE, canMsg)
	printCANStatus("CAN.Write", canStatus)
	printCANMsg(canMsg)

def readCANMsg():
	canMsg = CAN.Read(PCAN_DEVICE)
	
	#Polling is not recommend as PCAN is providing an event base impletation. 
	#However, polling is used for simplicity. 
	while canMsg[0] == PCAN_ERROR_QRCVEMPTY:
		time.sleep(0.0001)
		canMsg = CAN.Read(PCAN_DEVICE)

	printCANStatus("CAN.Read", canMsg[0])
	printCANMsg(canMsg[1])

	return canMsg[1]

def printCANMsg(canMsg):
	print("MSG_CLASS: " + str(GET_MSG_CLASS(canMsg.ID)) + " SRC_ADDR: " + str(GET_SRC_ADDR(canMsg.ID)) + " DST_ADDR: " + str(GET_DST_ADDR(canMsg.ID)))
	dataStr = "DATA: ";
	for i in range(canMsg.LEN):
		dataStr += str(canMsg.DATA[i]) + " "
	dataStr += "\n"
	print(dataStr)

def printCANStatus(fncStr, canStatus):
	canErrorMsg = CAN.GetErrorText(canStatus)
	print(fncStr + ": " + canErrorMsg[1])

def GET_MSG_CLASS(ID):
	return ((ID & 0x700) >> 8)

def GET_SRC_ADDR(ID):
	return ((ID & 0x0F0) >> 4)

def GET_DST_ADDR(ID):
	return (ID & 0x00F)

def GET_CAN_ID(MSG_CLASS, SRC_ADDR, DST_ADDR):
	return ((MSG_CLASS << 8) | (SRC_ADDR << 4) | DST_ADDR)

if __name__ == "__main__":
	main()