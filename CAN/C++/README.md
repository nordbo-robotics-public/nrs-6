Open a terminal in the current directory and use the following commands to compile and run the example. 
```sh
$ g++ ftsensorcan.cpp -o ftsensorcan -lpcanbasic
$ ./ftsensorcan
```