# Nordbo LRS6 Force-Torque Sensor ROS Node
Welcome to the ROS node for the Nordbo Robotics LRS6. It is assumed that the reader is familiar with a ROS and ROS nodes. 

The node has been tested on Ubuntu 18.04 LTS with ROS Melodic. 

## Installation instructions
1. Copy this folder into the src folder in your catkin workspace
2. Run the install_driver.sh script. This can be done by running the following in the terminal: 
``` chmod +x ./install_driver.sh  && ./install_driver.sh ```
3. Finally, run catkin_make, and the node should be good to go. 

## Using the node
The node is started using a roslaunch script. In a terminal, run the following: 

```
roslaunch nordbo_lrs6 nordbo_lrs6.launch
```

Remember to set the correct IP adresse of the force torque sensor in the launch file. The launch file is placed in the "launch" directory. 

### Topics:
Once the node is running, wrench measurements are published to topic: 
/NordboLRS6_node/wrench

### Services: 
At time of writing, 4 services exists: 

/NordboLRS6_node/setDataRate
/NordboLRS6_node/startStream
/NordboLRS6_node/stopStream
/NordboLRS6_node/tare

**/NordboLRS6_node/setDataRate**
Sets the datarate to a specific rate. Argument should be rate in Hz. 

** /NordboLRS6_node/startStream**  AND 
**/NordboLRS6_node/stopStream**
Are used to start and stop publishing to the wrench topic. 

**/NordboLRS6_node/tare**
Is used to tare the sensor - eg. set current wrench equal to 0. Should be called starting the task where wrench data is used (ie. before the robot starts polishing etc.)
