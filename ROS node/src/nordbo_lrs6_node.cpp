// Node
#include <iostream> // cout
#include <memory> // make_shared
#include "NordboFT/NordboLRS6.hpp" // NordboLRS6
#include <ros/init.h> // init
#include <ros/console.h> // ROS_INFO_STREAM

/**
 * @brief main
 * @param argc
 * @param argv Command-line arguments (parsed by ROS)
 * @return
 */
int main(int argc, char **argv)
{
    ros::init(argc,argv,"nordbo_lrs6_node");
    ROS_INFO_STREAM("Using Node version of NordboLRS6.");
    auto impl=std::make_shared<NordboLRS6>();
    impl->start();
    std::cout << "nordbo_lrs6_node exit\n";
    return 0;
}
