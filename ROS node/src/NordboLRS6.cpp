#include "NordboFT/NordboLRS6.hpp"


#include <string>
#include <chrono>
#include <thread>
#include <memory>

// Inline function to convert from rw wrench to ros wrench
geometry_msgs::Wrench wrench_to_ros(std::array<double,6> wrench)
{
    geometry_msgs::Wrench ros_wrench;
    ros_wrench.force.x = wrench[0];
    ros_wrench.force.y = wrench[1];
    ros_wrench.force.z = wrench[2];
    ros_wrench.torque.x = wrench[3];
    ros_wrench.torque.y = wrench[4];
    ros_wrench.torque.z = wrench[5];
    return ros_wrench;
}




bool NordboLRS6::startStreamCallback(std_srvs::Empty::Request &req,
                                     std_srvs::Empty::Response &res)
{
    ROS_INFO("Started stream!");
    return ftsensor.start();
}

bool NordboLRS6::stopStreamCallback(std_srvs::Empty::Request &req,
                                    std_srvs::Empty::Response &res)
{
    ROS_INFO("Stopped stream!");
    return ftsensor.stop();
}

bool NordboLRS6::tareCallback(std_srvs::Empty::Request &req,
                              std_srvs::Empty::Response &res)
{

    ROS_INFO("Tare!");
    return ftsensor.tare();
}

bool NordboLRS6::setDataRateCallback(nordbo_lrs6::setDataRate::Request &req,
                                     nordbo_lrs6::setDataRate::Request &res)
{
    ROS_INFO_STREAM("Setting time between measurements to: " << static_cast<int>(req.frequency) << " Hz!");

        double time_between_samples = 1000/static_cast<double>(req.frequency);
        return ftsensor.setDataRate(static_cast<int>(std::round(time_between_samples)));
}

void NordboLRS6::forceTorqueThread()
{

    // Setup a nice publisher


    std::cout << "Starting sensor!" << std::endl;

    ftsensor.start();


    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    std::array<double,6> wrench;
    unsigned int sequence_number = 0;

    // While ft sensor good, read and publish the readings!
    while(ros::ok())
    {
        try
        {
            wrench = ftsensor.getNextWrench();
        }
        catch (...)
        {
            std::cout << "Read failed" << std::endl;
        }
        geometry_msgs::WrenchStamped wrench_msg;

        wrench_msg.wrench = wrench_to_ros(wrench);
        // write header
        wrench_msg.header.seq = sequence_number++;
        wrench_msg.header.stamp = ros::Time::now();
        // publish message
        wrench_pub.publish(wrench_msg);

    }

    ftsensor.stop();

}




NordboLRS6::NordboLRS6(const ros::NodeHandle &nh, const ros::NodeHandle &prnh)
    :node_handle(nh)
    ,node_handle_priv(prnh)
    ,sensor_ip(   node_handle_priv.param("sensor_ip", std::string("192.168.0.100")))
    ,ftsensor(sensor_ip)
{
    ROS_INFO_STREAM("Sensor IP: " << sensor_ip);
}

void NordboLRS6::start() {


    // Set up wrench publisher
    wrench_pub = node_handle.advertise<geometry_msgs::WrenchStamped>(ros::this_node::getName() + "/wrench",100);

    ros::ServiceServer startStreamService = node_handle.advertiseService(ros::this_node::getName() + "/startStream", &NordboLRS6::startStreamCallback, this);
    ros::ServiceServer stopStreamService =  node_handle.advertiseService(ros::this_node::getName() + "/stopStream", &NordboLRS6::stopStreamCallback, this);
    ros::ServiceServer tareService =        node_handle.advertiseService(ros::this_node::getName() + "/tare", &NordboLRS6::tareCallback, this);
    ros::ServiceServer setDataRateService = node_handle.advertiseService(ros::this_node::getName() + "/setDataRate", &NordboLRS6::setDataRateCallback, this);

    //Start FTsensor thread
    std::unique_ptr<std::thread> ftsensor_thread(new std::thread(&NordboLRS6::forceTorqueThread,this));

    ros::spin();
}
