#include "NordboFT/nordbo_lrs6_nodelet.hpp"

#include <memory>
#include <nodelet/nodelet.h>
#include <ros/console.h>
#include "NordboFT/NordboLRS6.hpp"



#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(NordboLRS6Nodelet, nodelet::Nodelet)


NordboLRS6Nodelet::NordboLRS6Nodelet() : Nodelet(), impl() { }

NordboLRS6Nodelet::~NordboLRS6Nodelet() { }

void NordboLRS6Nodelet::onInit() {
    ROS_INFO_STREAM("Using Nodelet version of NordboLRS6.");
    impl=std::make_shared<NordboLRS6>(getNodeHandle(),getPrivateNodeHandle());
    impl->start();
}
