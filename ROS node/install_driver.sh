#!/bin/bash
echo "Nordbo Force-Torque sensor driver installer!"
echo "Needs sudo rights to perform the installation"

sudo dpkg -i "dependencies/RobWork_0.8.0_Ubuntu_18.04.1_LTS_x86_64_release.deb"
sudo dpkg -i "dependencies/robworkhardware_0.8.0-dev.deb"
sudo dpkg -i "dependencies/robworkhardware_0.8.0-nrs_eth.deb"
sudo dpkg -i "dependencies/robworkhardware_0.8.0-nrs_minimal_driver.deb"

echo "All good! Go ahead, run catkin_make." 
