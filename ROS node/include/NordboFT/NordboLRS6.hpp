#ifndef NordboLRS6_HPP
#define NordboLRS6_HPP

// At the least, you should include this header for the global and private node handles.
#include <ros/ros.h>
#include <ros/node_handle.h> // NodeHandle

#include <std_srvs/Empty.h>
#include <geometry_msgs/WrenchStamped.h>

#include <nordbo_lrs6/setDataRate.h>

#include <rwhw/lrs_6_eth/lrs6_minimal_driver.hpp>

#include <string>

/**
 * @brief The NordboLRS6 class Remember to document your interface.
 */
class NordboLRS6 {
protected:

    /** @brief node_handle Global node handle */
    ros::NodeHandle node_handle;

    /** @brief node_handle_priv Private node handle */
    ros::NodeHandle node_handle_priv;



    // Constants and variables

    /**
     * @brief imageCallback Image subscriber callback.
     * @param msg Using ImageConstPtr to allow copy-free memory sharing between Nodelets.
     */
    virtual bool startStreamCallback(std_srvs::Empty::Request &req,
                                     std_srvs::Empty::Response &res);

    virtual bool stopStreamCallback(std_srvs::Empty::Request &req,
                                    std_srvs::Empty::Response &res);

    virtual bool tareCallback(std_srvs::Empty::Request &req,
                              std_srvs::Empty::Response &res);

    virtual bool setDataRateCallback(nordbo_lrs6::setDataRate::Request &req,
                                     nordbo_lrs6::setDataRate::Request &res);
    /**
     * @brief forceTorqueThread holds the thread that interacts with FT sensor driver
     * should be started before calling ROS::SPIN
     */
    virtual void forceTorqueThread();

    ros::Publisher wrench_pub;
    std::string sensor_ip;
    lrs6_minimal_driver::lrs6 ftsensor;

public:

    /**
     * @brief NordboLRS6 This Nodes' default constructor
     * @param nh Global node handle
     * @param prnh Private node handle
     */
    NordboLRS6(
            const ros::NodeHandle &nh = ros::NodeHandle(),
            const ros::NodeHandle &prnh = ros::NodeHandle("~")
            );

    /**
     * @brief ~NordboLRS6 Destructor ensuring clean shutdown of this node.
     */
    virtual ~NordboLRS6() { }

    /**
     * @brief start Required method
     */
    virtual void start();
};


#endif // NordboLRS6_HPP
