#ifndef NordboLRS6NODELET_HPP
#define NordboLRS6NODELET_HPP

// Nodelet
#include <memory>
#include <nodelet/nodelet.h>
#include "NordboFT/NordboLRS6.hpp"

/**
 * @brief The NordboLRS6Nodelet class Minimal wrapper for @ref NordboLRS6
 */
class NordboLRS6Nodelet : public nodelet::Nodelet {
protected:
    /** @brief impl Pointer type delays construction until @ref onInit() .  */
    std::shared_ptr<NordboLRS6> impl;
public:
    /** @brief NordboLRS6Nodelet Nothing happens in this constructor. */
    NordboLRS6Nodelet();
    /** @brief ~NordboLRS6Nodelet Destructor. Remember @ref NordboLRS6 destructor is called. */
    virtual ~NordboLRS6Nodelet();
    /** @brief onInit Full @ref NordboLRS6 initialization using node handles. */
    virtual void onInit();
};

#endif // NordboLRS6NODELET_HPP
